#include <stdio.h>
#include <stdint.h>


const int8_t rows = 5;
const int8_t cols = 4;

int8_t grid[5][4] = 
	{{1,1,0,1},
	 {0,0,1,0},
	 {0,0,0,0},
	 {1,1,1,1}};

void floodfill(int8_t row, int8_t col, int8_t grid[][cols]) {
	if(grid[row][col] == 1) {
		grid[row][col] = 0;
		if(row+1 < rows)
			floodfill(row+1, col, grid);
		if(row-1 >= 0)
			floodfill(row-1, col, grid);
		if(col+1 < cols)
			floodfill(row, col+1, grid);
		if(col-1 >= 0)
			floodfill(row, col-1, grid);
	}
}

int8_t find_nRegions(int8_t rows, int8_t cols, int8_t grid[][cols]) {
	uint8_t n = 0;

	for (uint8_t r = 0; r < rows; r++) {
		for (uint8_t c = 0; c < cols; c++) {
			if (grid[r][c] == 1) {
				n += 1;
				floodfill(r,c, grid);
			}
		}
	}
	return n;
}

int main() {
	printf("nRegions: %d\n",find_nRegions(rows, cols, grid));
}
